import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();

    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(6, 9);
        System.out.println(g);
        Graph clone = g.deepClone();
        System.out.println("---------------------------------------------");
        g.cloneProof(g, clone);
        System.out.println("---------------------------------------------");
        System.out.println("Finding the radius of the graph:");
        //EGERT
        g.findRadius();
        g.printRadiusInfo();
        System.out.println("---------------------------------------------");
        System.out.println("Finding cycles of the graph:");
        //VILLEM
        g.findAllCycles();
        g.printCycles();
        //KRISTJAN
        System.out.println("\n");
        System.out.println("Finding center of the free tree:");
        System.out.println(g.tree);
    }

    /**
     * Vertex represents one point/node in the graph and vertices are
     * connected by Arc objects.
     */
    class Vertex {

        /**
         * String value of Vertex.
         */
        private String id;
        /**
         * Next vertex to this.vertex.
         */
        private Vertex next;
        /**
         * First arc to this.vertex.
         */
        private Arc first;
        /**
         * Number of this.vertex
         * also described in id.
         */
        private int VertNum;
        /**
         * The maximum distance between this.vertex to all other vertices.
         */
        private int eccentricity;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s, int idNum) {
            this(s, null, null);
            VertNum = idNum - 1;
        }

        Vertex(String s) {
            this(s, null, null);
        }


        public Vertex clone(){
            Vertex help = new Vertex(id, VertNum + 1);
            help.first = first;
            help.next = next;
            return help;
        }

        @Override
        public String toString() {
            return id;
        }

        /**
         * Add arc to vertex.
         * If there is already an arc in first iterate through
         * next until next arc is null.
         *
         * @param toAdd arc that will be added to vertex.
         */
        public void addArc(Arc toAdd) {
            if (first == null) {
                first = toAdd;
                return;
            }
            Arc last = first;
            while (last.next != null) {
                last = last.next;
            }
            last.next = toAdd;
        }

    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        /**
         * String value of Arc.
         */
        private String id;
        /**
         * Target vertex of this.arc.
         */
        private Vertex target;
        /**
         * Next arc to this.arc.
         */
        private Arc next;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }


        //VILLEM
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Arc arc = (Arc) o;
            String[] tempa = arc.id.split("[a_]");
            String[] tempb = id.split("[a_]");
            return (tempa[1].equals(tempb[2]) && tempa[2].equals(tempb[1])) || id.equals(arc.id);
        }

        @Override
        public String toString() {
            return id;
        }

    }


    /**
     * A graph is defined as set of points known as Vertices and line joining these points is known as Arcs.
     */
    class Graph {

        /**
         * String value of this graph.
         */
        private String id;
        /**
         * First vertex to this.graph.
         */
        private Vertex first;
        /**
         * The number of Vertices this.graph has.
         */
        private int numOfVertices = 0;
        /**
         * Adjacency of this graph's vertices.
         * Each vertex has it's own list to display to which vertex.VertNum it is connected to.
         */
        private ArrayList<ArrayList<Integer>> adjacency;
        /**
         * List of all vertices
         */
        private Vertex[] vert;
        /**
         * Radius of the graph.
         * Minimum of all vertices eccentricities.
         */
        private int radius;

        //KRISTJAN
        private List<Vertex> tree;

        //VILLEM
        private List<List<Arc>> cycles = new ArrayList<>();

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }


        public Vertex createVertex(String vid, int numOfVert) {
            Vertex res = new Vertex(vid, numOfVert);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            adjacency = new ArrayList<>();
            numOfVertices = n;
            for (int i = 0; i < n; i++) {
                adjacency.add(new ArrayList<>());
            }
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i), n - i);
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    int vI = varray[i].VertNum; // source vertex Number
                    int vJ = varray[vnr].VertNum; // target vertex Number
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                    adjacency.get(vI).add(vJ); // Adding target vertex number to source vertex list
                    adjacency.get(vJ).add(vI); // Adding source vertex number to target vertex list
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            int[][] res = new int[numOfVertices][numOfVertices];
            Vertex v = first;
            while (v != null) {
                int i = v.VertNum;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.VertNum;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param vertices number of vertices
         * @param edges    number of edges
         */
        public void createRandomSimpleGraph(int vertices, int edges) {

            first = null;
            createRandomTree(vertices);       // n-1 edges created here
            tree = this.findCenter();
            vert = new Vertex[vertices];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = edges - vertices + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * vertices);  // random source
                int j = (int) (Math.random() * vertices);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i]; // source vertex
                Vertex vj = vert[j]; // target vertex
                int viId = vi.VertNum; // source vertex Number
                int vjId = vj.VertNum; // target vertex Number
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                adjacency.get(viId).add(vjId); //
                adjacency.get(vjId).add(viId); //
                edgeCount--;  // a new edge happily created
            }
        }


        /**
         * Find the eccentricity for each vertex in this.graph.
         * Find the minimum of all the eccentricities and declare it as a radius to this.graph.
         */
        private void findRadius() {
            radius = Integer.MAX_VALUE;

            if (vert == null) {
                throw new RuntimeException("Can not find radius, 0 vertices given");
            }

            for (Vertex v : vert) {
                int vNum = v.VertNum;
                v.eccentricity = breadthFirstSearch(vNum);
                if (v.eccentricity < this.radius) {
                    this.radius = v.eccentricity;
                }
            }
        }

        /**
         * @return  the radius for this.graph.
         */
        private int getRadius() {
            return this.radius;
        }


        /**
         * Print out all of the vertices eccentricities and the radius for this.graph.
         */
        private void printRadiusInfo() {
            StringBuffer sb = new StringBuffer();
            String nl = System.getProperty("line.separator");
            for (Vertex v : vert) {
                sb.append("Vertex ").append(v.toString()).append(" has the eccentricity of ").append(v.eccentricity);
                sb.append(nl);
            }
            sb.append(nl);
            sb.append("Graph has a radius of ").append(getRadius());
            sb.append(" because it is the lowest of all vertices eccentricities");
            System.out.println(sb);
        }


        /**
         * Find the the maximum among shortest collaboration distances between source and every other vertex
         * using the breadth-first search.
         * @param source number of the vertex from where to start the search.
         * @return  eccentricity of source.
         */
        /* Inspiratsioon allikatest:
        https://www.geeksforgeeks.org/shortest-path-unweighted-graph/
        https://github.com/ambye85/sedgewick_algorithms/blob/master/src/main/java/uk/ashleybye/sedgewick/
        graph/Eccentricity.java
        https://www.youtube.com/watch?v=TIbUeeksXcI */
        private int breadthFirstSearch(int source) {
            LinkedList<Integer> queue = new LinkedList<>();
            int[] isVisisted = new int[numOfVertices];
            int[] distanceToSource = new int[numOfVertices];

            isVisisted[source] = 1;
            distanceToSource[source] = 0;
            queue.add(source);

            int eccentricity = 0;

            while (!queue.isEmpty()) {
                int vertex = queue.remove();
                for (int adjacentVertex : adjacency.get(vertex)) {
                    if (isVisisted[adjacentVertex] == 0) {
                        distanceToSource[adjacentVertex] = distanceToSource[vertex] + 1;
                        isVisisted[adjacentVertex] = 1;
                        queue.add(adjacentVertex);

                        if (distanceToSource[adjacentVertex] > eccentricity) {
                            eccentricity = distanceToSource[adjacentVertex];
                        }
                    }
                }
            }
            return eccentricity;
        }

        /**  method to start a method that finds all possible cycles
         *  @return collection of cycles
         */
        public List<List<Arc>> findAllCycles() {
            findCycles(new Stack<>());
            return cycles;
        }

        /**  Finds out if the observable arc is in the traveled through list
         *  @param traveledThrough traveled through the arcs of which are the potential causes of the cycles.
         *  @param observableArc arc which may be a potential cause of the cycle
         *  @return index of the cycle start or -1 what indicating there is no cycle
         */
        public int detectCycle(List<Arc> traveledThrough, Arc observableArc) {
            int index = 0;
            for (Arc arc : traveledThrough) {
                String[] tempa = arc.id.split("[a_]");
                if (tempa[1].equals(observableArc.target.id)) {
                    return index;
                }
                index++;
            }
            return -1;
        }

        /** Method that find out if the observable cycle is already in cycles list
         *  @param observableCycle list of vertices that make up the cycle
         */
        private boolean containCycle(List<Arc> observableCycle) {
            for (List<Arc> cycle : cycles) {
                if (cycle.size() == observableCycle.size()) {
                    int counter = 0;
                    for (Arc arc : observableCycle) {
                        if (cycle.contains(arc)) {
                            counter++;
                        }
                    }
                    if (counter == cycle.size()) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**  Method that search cycles
         *  @param traveledThrough traveled through the arcs of which are the potential causes of the cycles.
         */
        private void findCycles(List<Arc> traveledThrough){
            if (first == null) {
                return;
            }
            if (traveledThrough.size() > 1) {
                Arc lastArc = traveledThrough.get(traveledThrough.size() - 1);
                List<Arc> observableArcsList = traveledThrough.subList(0, traveledThrough.size() - 2);
                int cycleFromIndex = detectCycle(observableArcsList, lastArc);
                if (cycleFromIndex != -1) {
                    List<Arc> cycle = traveledThrough.subList(cycleFromIndex, traveledThrough.size());
                    if (!containCycle(cycle)) {
                        cycles.add(new ArrayList<>(cycle));
                    }
                    return;
                }
            }

            Arc observableArc = first.first;
            if (observableArc == null) {
                return;
            }
            while (true) {
                if (traveledThrough.size() < 1 || !traveledThrough.contains(observableArc)) {
                    traveledThrough.add(observableArc);
                    first = observableArc.target;
                    findCycles(traveledThrough);
                    traveledThrough.remove(traveledThrough.size() - 1);
                }

                if (observableArc.next == null) {
                    break;
                }
                observableArc = observableArc.next;
            }

        }

        /** Print all cycles
         */
        private void printCycles() {
            int cycleIndex = 1;
            for (List<Arc> cycle: cycles) {
                System.out.println("\nCycle nr" + cycleIndex + ":");
                int vertexIndex = 1;
                int cycleSize = cycle.size();
                for (Arc arc : cycle) {
                    System.out.print(arc.id);
                    if (vertexIndex < cycleSize) {
                        System.out.print(", ");
                    }
                    vertexIndex++;
                }
                cycleIndex++;
            }
        }

        //KRISTJAN
        private int treeNodeCount(){
            int counter = 0;
            Vertex v = this.first;
            while (v != null) {
                v.VertNum = 0;
                counter++;
                v = v.next;
            }
            return counter;
        }

        //Return list of numbers consisting each Vertex's amount of arcs.
        private List<Integer> degrees(){

            List<Integer> degrees = new ArrayList<>(this.treeNodeCount());
            Vertex v = first;
            while (v.next != null){

                Arc a = v.first;
                while (a != null){
                    v.VertNum++;
                    a = a.next;
                }
                if(v.next != null){
                    degrees.add(v.VertNum);
                }
                v = v.next;
            }
            Arc a = v.first;
            while (a != null){
                v.VertNum++;
                a = a.next;
            }
            degrees.add(v.VertNum);
            return degrees;
        }
        private List<Vertex> createGraphList(){
            List<Vertex> graph = new ArrayList<>(this.treeNodeCount());
            Vertex v = first;
            while(v.next != null){
                graph.add(v);
                v = v.next;
            }
            graph.add(v);
            return graph;
        }

        //Inspiratsioon lehekyljelt https://towardsdatascience.com/graph-theory-center-of-a-tree-a64b63f9415d
        public List<Vertex> findCenter(){
            int numberOfNodes = this.treeNodeCount();
            List<Vertex> graph = createGraphList();
            List<Integer> degree = degrees();
            List<Vertex> leaves = new ArrayList<>();

            //Disregard Vertexes with only one arc as leaves.
            for (int i = 0; i < numberOfNodes; i++) {
                if (degree.get(i) <= 1){
                    leaves.add(graph.get(i));
                    degree.set(i, 0);
                }
            }
         /*//Using disregarded leaves, removes disregarded arc from leaf. Once leaf has one arc, disregards that.
         Cycle repeats til there are two canditates for center.
         */
            int count = leaves.size();
            while(count < numberOfNodes){
                List<Vertex> new_leaves = new ArrayList<>();
                for (int i = 0; i < leaves.size(); i++) {
                    Vertex vertex = leaves.get(i);
                    for (int j = 0; j < graph.size(); j++) {
                        if(graph.get(j).equals(vertex)){
                            Vertex neighbour = graph.get(j).first.target;
                            int neighbourIndex = graph.indexOf(neighbour);
                            degree.set(neighbourIndex, degree.get(neighbourIndex)- 1);
                            neighbour.first = neighbour.first.next;
                            if(degree.get(neighbourIndex) == 1){
                                new_leaves.add(neighbour);
                            }
                        }
                    }
                }
                count += new_leaves.size();
                leaves = new_leaves;
            }
            return leaves;
        }

        public Graph deepClone() {
            /*
             * Solution is inspired from 2 sources from the same author.
             * Video explanation https://www.youtube.com/watch?v=vma9tCQUXk8
             * GitHub: https://github.com/codewithcs/backtobackswe/blob/master/Graphs/CloneAGraph/CloneAGraph.java
             * */
            if (first == null) {
                return new Graph(id);
            }

            Map<Vertex, Vertex> vertexMap = new HashMap<>(); // Map to hold vertices <OriginalVertex, ClonedVertex>

            Stack<Vertex> stack = new Stack<>(); // Stack to create cloned vertices with proper references

            stack.push(first);

            vertexMap.put(first, new Vertex(first.id));

            Vertex current = first;

            while (current.next != null) {
                Vertex parent = stack.pop();
                vertexMap.put(current.next, new Vertex(current.next.id));
                vertexMap.get(parent).next = vertexMap.get(current.next);
                stack.push(parent);
                stack.push(current.next);
                current = current.next;
            }


            // Iterating over stack of all vertices to create arcs for every vertex.
            while (!stack.isEmpty()) {
                Vertex currVertex = stack.pop();

                Arc vertexArc = currVertex.first;

                while (vertexArc != null) {
                    String arcString = "a" + vertexMap.get(currVertex).id + "_" + vertexMap.get(vertexArc.target).id;
                    vertexMap.get(currVertex).addArc(new Arc(arcString, vertexMap.get(vertexArc.target), null));
                    vertexArc = vertexArc.next;
                }
            }

            return new Graph(id, vertexMap.get(first));
        }

        public void cloneProof(Graph original, Graph clone){
            Vertex originalVertex = original.first;
            Vertex clonedVertex = clone.first;
            while (originalVertex != null && clonedVertex != null){
                Arc originalArc = originalVertex.first;
                Arc clonedArc = clonedVertex.first;
                System.out.println("originalVertex id: " + originalVertex.id +  " clonedVertex id: " + clonedVertex.id);
                System.out.println("originalVertex is the same object as clonedVertex: " +
                        String.valueOf(originalVertex == clonedVertex));
                System.out.println("originalVertex is the same class as clonedVertex: " +
                        String.valueOf(originalVertex.getClass() == originalVertex.getClass()));
                while (originalArc != null && clonedArc != null){
                    System.out.println("originalArc id: " + originalArc.id +  " clonedArc id: " + clonedArc.id);
                    System.out.println("originalArc is the same object as clonedArc: " +
                            String.valueOf(originalArc == clonedArc));
                    System.out.println("originalArc is the same class as clonedArc: " +
                            String.valueOf(originalArc.getClass() == clonedArc.getClass()));
                    originalArc = originalArc.next;
                    clonedArc = clonedArc.next;
                }
                originalVertex = originalVertex.next;
                clonedVertex = clonedVertex.next;
            }
        }

    }
}

